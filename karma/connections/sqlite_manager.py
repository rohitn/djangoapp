import sqlite3

class ConnectionManager(object):

  def __init__(self):
    self.conn = sqlite3.connect("karmadb")

  def ExecuteSelect(self, command):
    cursor = self.conn.cursor()
    cursor.execute(command)
    result = []
    for row in cursor:
      result.append(row)
    cursor.close()
    return result

  def ExecuteCommand(self, command):
    cursor = self.conn.cursor()
    cursor.execute(command)
    self.conn.commit()

  def CloseConnection(self):
    self.conn.close()