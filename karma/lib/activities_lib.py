from datetime import datetime
from karma.connections import sqlite_manager
from karma.models import Activity

def GetActivities(activity_ids=None):
  connection = sqlite_manager.ConnectionManager()
  if activity_ids:
    command = """
    SELECT * FROM Activity
    WHERE activity_id IN (%s)
    """ % (",".join(map(str, activity_ids)))
  else:
    command = """
    SELECT * FROM Activity
    """
  data = connection.ExecuteSelect(command)
  activities = []
  for row in data:
    activity = Activity(row[0])
    activity.user_id = row[1]
    activity.title = row[2]
    activity.location = row[3]
    activity.description = row[4]
    activity.activity_date = row[5]
    activity.value = row[6]
    activity.is_completed = row[7]
    activity.is_private = row[8]
    activity.status = row[9]
    activity.created_at = row[10]
    activity.updated_at = row[11]
    activities.append(activity)
  connection.CloseConnection()
  return activities