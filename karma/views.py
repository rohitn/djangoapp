from django.core import serializers
from django.utils import simplejson
from django.http import HttpResponse
from django.http import HttpResponseBadRequest
from karma.lib import activities_lib
from karma.models import Activity
import datetime
import time

def item_all(request):
  activities = activities_lib.GetActivities()
  json_output = []
  for activity in activities:
    json_output.append(activity.__dict__)
  data = simplejson.dumps(json_output)
  return HttpResponse(data, mimetype="application/json")

def item_new(request):
  # the request type has to be POST
  if request.method != "POST":
    return HttpResponseBadRequest("Request has to be of type POST")
  rawdata = request.POST[u"data"]
  postdata = simplejson.loads(rawdata)
  activity = Activity()
  activity.activity_id = int(time.time()*1e6)
  activity.user_id = postdata["user_id"]
  activity.title = postdata["title"]
  activity.location = postdata["location"]
  activity.description = postdata["description"]
  activity.activity_date = postdata["activity_date"]
  activity.value = postdata["value"]
  activity.is_completed = postdata["is_completed"]
  activity.is_private = postdata["is_private"]
  activity.status = postdata["status"]
  activity.created_at = datetime.datetime.now().isoformat()
  activity.updated_at = datetime.datetime.now().isoformat()
  activity.Create()
  retrieved_activity = Activity(activity.activity_id)
  retrieved_activity.Get()
  data = simplejson.dumps(retrieved_activity.__dict__)
  return HttpResponse(data, mimetype="application/json")

def item(request, activity_id):
  activity = Activity(int(activity_id))
  activity.Get()
  if request.method == "GET":
    data = simplejson.dumps(activity.__dict__)
    return HttpResponse(data, mimetype="application/json")
  elif request.method == "POST":
    rawdata = request.POST[u"data"]
    postdata = simplejson.loads(rawdata)
    activity.user_id = postdata["user_id"]
    activity.title = postdata["title"]
    activity.location = postdata["location"]
    activity.description = postdata["description"]
    activity.activity_date = postdata["activity_date"]
    activity.value = postdata["value"]
    activity.is_completed = postdata["is_completed"]
    activity.is_private = postdata["is_private"]
    activity.status = postdata["status"]
    activity.created_at = postdata["created_at"]
    activity.updated_at = datetime.datetime.now().isoformat()
    activity.Update()
    data = simplejson.dumps(True)
    return HttpResponse(data, mimetype="application/json")
  elif request.method == "DELETE":
    item.Delete()
    data = simplejson.dumps(True)
    return HttpResponse(data, mimetype="application/json")
  else:
    return HttpResponseBadRequest("Invalid request")
