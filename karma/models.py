from datetime import datetime
from karma.connections import sqlite_manager

class Activity(object):

  def __init__(self, activity_id):
    self.activity_id = activity_id
    self.user_id = 0
    self.title = None
    self.location = None
    self.description = None
    self.activity_date = None
    self.value = 0
    self.is_completed = False
    self.is_private = False
    self.status = None
    self.created_at = None
    self.updated_at = None
    self.exists = False

  def __unicode__(self):
    return self.title
  
  def Get(self):
    connection = sqlite_manager.ConnectionManager()
    command = """
    SELECT * FROM Activity WHERE activity_id = %d
    """ % (self.activity_id)
    result = connection.ExecuteSelect(command)
    connection.CloseConnection()
    if result:
      self.user_id = result[0][1]
      self.title = result[0][2]
      self.location = result[0][3]
      self.description = result[0][4]
      self.activity_date = result[0][5]
      self.value = result[0][6]
      self.is_completed = result[0][7]
      self.is_private = result[0][8]
      self.status = result[0][9]
      self.created_at = result[0][10]
      self.updated_at = result[0][11]
      self.exists = True

  def Create(self):
    connection = sqlite_manager.ConnectionManager()
    command = """
    INSERT INTO Activity (activity_id, user_id,
    title, location, description, activity_date, value,
    is_completed, is_private, status, created_at, updated_at)
    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
    """ % (self.activity_id, self.user_id, self.title, self.location,
           self.description, self.activity_date, self.value,
           self.is_completed, self.is_private, self.status,
           self.created_at, self.updated_at)
    connection.ExecuteCommand(command)
    connection.CloseConnection()
  
  def Update(self, activity_id):
    connection = sqlite_manager.ConnectionManager()
    command = """
    UPDATE Activity SET activity_id = %s, user_id = %s,
    title = %s, location = %s, description = %s, activity_date = %s,
    value = %s, is_completed = %s, is_private = %s, status = %s,
    created_at = %s, updated_at = %s
    WHERE activity_id = %d
    """ % (self.activity_id, self.user_id, self.title, self.location,
           self.description, self.activity_date, self.value,
           self.is_completed, self.is_private, self.status,
           self.created_at, self.updated_at, activity_id)
    connection.ExecuteCommand(command)
    connection.CloseConnection()
    
  def Delete(self, activity_id):
    self.status = "inactive"
    connection = sqlite_manager.ConnectionManager()
    command = """
    UPDATE Activity SET activity_id = %s, user_id = %s,
    title = %s, location = %s, description = %s, activity_date = %s,
    value = %s, is_completed = %s, is_private = %s, status = %s,
    created_at = %s, updated_at = %s
    WHERE activity_id = %d
    """ % (self.activity_id, self.user_id, self.title, self.location,
           self.description, self.activity_date, self.value,
           self.is_completed, self.is_private, self.status,
           self.created_at, self.updated_at, activity_id)
    connection.ExecuteCommand(command)
    connection.CloseConnection()
