from django.conf.urls import patterns, include, url


urlpatterns = patterns('',
    # The normal jazz here...

     (r'^item/(?P<activity_id>\d+)/$', "karma.views.item"),
    # will be used to get info about all items
    (r'^item/all/$', "karma.views.item_all"),
    # will be used to create new item
    (r'^item/new/$', "karma.views.item_new"),
)
